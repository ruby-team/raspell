raspell (1.3-3) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.4.1, no changes needed.
  * Update watch file format version to 4.
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libaspell-dev.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 24 Feb 2020 01:12:10 +0000

raspell (1.3-2) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 4.4.0 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Drop compat file, rely on debhelper-compat and bump compat level to 12
  * Drop libraspell* obsolete packages (Closes: #878854)
  * Drop unused lintian warnings
  * Set Testsuite to autopkgtest-pkg-ruby

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Cédric Boutillier <boutil@debian.org>  Wed, 04 Sep 2019 23:14:35 +0200

raspell (1.3-1) unstable; urgency=medium

  * Adption of the package by the Ruby Extras Maintainers team
  * New upstream version 1.3
  * Improve description (Closes: #624242)
  * Transition to the new Ruby policy (Closes: #675114, #722374)
  * Make libraspell-ruby* transitional packages and provide a raspell
    binary package
  * Build-Depend on aspell-en and Recommends aspell-dictionary
  * Add patches to fix format-security issues and add an explicit
    dictionary for tests
  * debian/copyright: use copyright-format-1.0
  * Add lintian-overrides about duplicated descriptions of transitional
    packages

 -- Cédric Boutillier <boutil@debian.org>  Wed, 15 Jan 2014 14:01:19 +0100

raspell (1.2-2) unstable; urgency=low

  * Fix "FTBFS: ext/raspell.c:76:9: error: format not a string literal
    and no format arguments [-Werror=format-security]." Thanks to Lucas
    Nussbaum and Andreas Stuhrk. (Closes: #676077)

 -- Alex Pennace <alex@pennace.org>  Sun, 17 Jun 2012 19:04:38 -0400

raspell (1.2-1) unstable; urgency=low

  * Initial release (Closes: #616189)

 -- Alex Pennace <alex@pennace.org>  Wed, 02 Mar 2011 21:54:43 -0500
